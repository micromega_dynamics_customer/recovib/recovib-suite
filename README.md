# RECOVIB Suite Software
<center><img src="Images/SuiteLauncher.PNG" width="70%"></center>

## Contents
<!-- MarkdownTOC autolink="true" bracket="round" markdown_preview="markdown" -->
- [Useful links](#useful-links)
- [Install](#install)
	- [Minimum requirements](#minimum-requirements)
	- [Prerequisites](#prerequisites)
- [Update](#update)
- [Tutorial](#tutorial)
- [Troubleshooting](#troubleshooting)
	- [RECOVIB Converter Issues](#recovib-converter-issues)
		- [MatFile export issues](#matfile-export-issues)
	- [RECOVIB Suite Issues](#recovib-suite-issues)
		- [Anaconda Suite compatibility](#anaconda-suite-compatibility)
- [Versions History](#versions-history)

<!-- /MarkdownTOC -->
## Useful links
- [Recovib Monitor Documentation](https://gitlab.com/micromega_dynamics_customer/recovib/recovib-monitor/-/blob/master/Documentation/README.md)
- [RECOVIB Suite Tutorial Videos](https://www.youtube.com/watch?v=1GhClrhOoOE&list=PL6YQ74DilvOpptyQA2PogFPlM-L3hgg09)

## Install
To install the RECOVIB Suite, simply download the [latest version](Releases/RECOVIBSuiteBundle.exe) and run it. 

### Minimum requirements

- Windows 7 or later
- 64-bit platform

**Note** : The installation process is very straightforward and should not take more than a few minutes on recent platforms. On older platforms (e.g. Windows 7), longer installation processes were sometimes observed.

### Prerequisites

Several prerequisites will be automatically installed :
- DirectX Runtime
- Microsoft .NET Framework 4.5
- Visual C++ Redistributable for Visual Studio 2013 Update 5 x64
- Visual C++ Redistributable for Visual Studio 2013 Update 5 x86

## Update
At RECOVIB Suite launcher opening, you will be prompted with available updates if any.
You can always manually check by clicking the *Check for updates* button.

## Tutorial

A playlist of short RECOVIB Suite [Tutorial Videos](https://www.youtube.com/watch?v=1GhClrhOoOE&list=PL6YQ74DilvOpptyQA2PogFPlM-L3hgg09) is available on Youtube.

## Troubleshooting

### RECOVIB Converter Issues

#### MatFile export issues

If you get the following kind of problem, get to :

*{your RECOVIB project}\basic\parameters.txt* and modify the first entry of each line so that it contains only letters, digits or underscores. 

E.g :

```
Channel#Gain#Offset#Units#MaxRange#MinRange
This will fail#0.0000007408#24620.0000000000#g#6.1961600000#-6.2326390000
```

becomes

```
Channel#Gain#Offset#Units#MaxRange#MinRange
This_will_not_fail#0.0000007408#24620.0000000000#g#6.1961600000#-6.2326390000
```

![MatFile issue](Images/MATFILE_BUG.PNG)

### RECOVIB Suite Issues

#### Anaconda Suite compatibility

**v3.0.2** and older could lead to problem when using the [Anaconda Suite](https://www.anaconda.com).  
The problem is fixed by deleting the following files (this can require administrator permissions) in the **System32** folder :

* C:\Windows\System32\hdf5_hl.dll
* C:\Windows\System32\hdf5.dll
* C:\Windows\System32\libiomp5md.dll
* C:\Windows\System32\mkl_custom.dll
* C:\Windows\System32\szip.dll
* C:\Windows\System32\zlib.dll

The **System32** folder can be found easily by typing *%windir%* in the Windows search.

## Versions History
* [**Current Version**](Releases/RECOVIBSuiteBundle.exe)
* **v3**
	* **v3.0**
		* [**v3.0.4**](Releases/Oldies/RECOVIBSuiteBundle_3_0_4.exe)
			- FEEL, MONITOR, VIEWER:
				- General stability improvements
			- FEEL, MONITOR, TINY:
				- Bug with regional formatting of datetimes fixed
		* [**v3.0.3**](Releases/Oldies/RECOVIBSuiteBundle_3_0_3.exe)
			- CONVERTER:
				- Compatibility with Anaconda Suite Software
		* [**v3.0.2**](Releases/Oldies/RECOVIBSuiteBundle_3_0_2.exe)
			- MONITOR, FEEL, VIEWER:
				- Statistics improvements
				- General Stability improvements
		    - MONITOR:
		      - Only channels wired to a connector are displayed
		    - VIEWER:
		      - End of file detection bug fixed
        * [**v3.0.1**](Releases/Oldies/RECOVIBSuiteBundle_3_0_1.exe)
        	- Update mechanism fully tested 
        * [**v3.0.0**](Releases/Oldies/RECOVIBSuiteBundle_3_0_0.exe)
        	- New Release System
		  	- MONITOR:
		  		- Monitor 4, 8, 16, 24 support added
		  		- Support for multiple sessions
		  		- CSV import for time domain
		  		- FFT size up to 16384 samples
		  		- Downsampling capabilities added
		  		- Custom Recording settings added 
		  	- FEEL:
		  		- CSV import for time domain
		  		- FFT size up to 16384 samples
		  		- Downsampling capabilities added
		  		- Custom Recording settings added 
		  	- VIEWER:
		  		- CSV import for time domain
		  		- FFT size up to 16384 samples
* **v2**
    * **v2.0**
        * [**v2.0.0**](Releases/Oldies/RECOVIBSuiteBundle_2_0_0.msi)
            - Deprecated and discontinuous version
